#!/usr/bin/python
from __future__ import annotations

import os
import socket
import time
import urllib.request
from functools import lru_cache
from pathlib import Path
from typing import TYPE_CHECKING

import cloudflare

import logging.config

if TYPE_CHECKING:
    from typing import Self, Any


log = logging.getLogger(__name__)

import click


@click.command
@click.version_option(version="0.1")
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase log level. '-v' will output INFO messages. "
    "More than -vv is useless.",
)
@click.option(
    "-q",
    "--quiet",
    count=True,
    help="Reduce log level. '-q' will suppress WARNING messages. "
    "More than -qq is useless.",
)
@click.option(
    "-l",
    "--log-file",
    help="Reduce log level. '-q' will suppress WARNING messages. "
    "More than -qq is useless.",
)
@click.argument("domain_names", nargs=-1)
@click.pass_context
def main(ctx: click.Context, verbose: int, quiet: int, log_file: str, domain_names) -> None:
    verbosity: int = int(logging.INFO / 10) + verbose - quiet
    logging.config.dictConfig(
        logging_configuration(verbosity, Path(log_file) if log_file else None)
    )

    log.info("Preparing CloudFlare API object")
    cf = CloudFlare.from_env()

    delay_in_minutes = 10

    while True:
        try:
            log.info("Getting public IP address")
            ip = get_current_ip()
            for domain_name in domain_names:
                log.info("Updating DNS record: %s -> %s", domain_name, ip)
                update_ip(cf,  domain_name, ip)
        except Exception as e:
            log.exception("Something went wrong")

        log.info("Sleeping %s minutes", delay_in_minutes)
        time.sleep(60 * delay_in_minutes)


def update_ip(cf: CloudFlare, domain_name: str, ip_address: str | None = None):
    ip_address = ip_address or get_current_ip()

    existing_dns_records = cf.dnsrecords_for_domain(domain_name)
    created_or_updated_records = []

    if not existing_dns_records:
        new_record = cf.create_dnsrecord(domain_name, ip_address)
        created_or_updated_records.append(new_record)
    else:
        for dns_record in existing_dns_records:
            if cf.ip_address_type(ip_address) != dns_record["type"]:
                # only update the correct address type (A or AAAA)
                # we don"t see this becuase of the search params above
                log.warning("IGNORED: %s %s ; wrong address family", domain_name, dns_record["content"])
                continue

            if ip_address == dns_record["content"]:
                log.info("UNCHANGED: %s %s", domain_name, ip_address)
                continue

            new_record = cf.update_dnsrecord(dns_record, domain_name, ip_address)
            created_or_updated_records.append(new_record)
            log.info("UPDATED: %s %s -> %s", domain_name, dns_record["content"], ip_address)
    return created_or_updated_records


class CloudFlare:
    def __init__(self,
             cf: cloudflare.CloudFlare
    ):
        self.cf = cf

    @classmethod
    def from_token(cls, token: str) -> Self:
        return cls(cloudflare.CloudFlare(token=token))

    @classmethod
    def from_env(cls) -> Self:
        for var_name in [
            "CLOUDFLARE_EMAIL",
            "CLOUDFLARE_API_KEY",
            "CLOUDFLARE_API_TOKEN",
            "CLOUDFLARE_API_CERTKEY",
        ]:
            if os.getenv(var_name, ""):
                log.info("%s environnement variable is defined and non-empty", var_name)
        return cls(cloudflare.Cloudflare())

    def update_dnsrecord(self, old_record, domain_name: str, ip_address: str):
        dns_record = {
            "name": domain_name,
            "type": self.ip_address_type(ip_address),
            "content": ip_address,
            "proxied": old_record["proxied"],
        }
        zone_id = self.zone_id_from_domain(domain_name)
        try:
            new_record = self.cf.zones.dns_records.put(zone_id, old_record["id"], data=dns_record)
            return new_record
        except cloudflare.CloudFlareAPIError as e:
            raise RuntimeError("PUT /zones/dns_records %s - api call failed", domain_name) from e

    def create_dnsrecord(self, domain_name: str, ip_address: str):
        dns_record = {
            "name": domain_name,
            "type": self.ip_address_type(ip_address),
            "content": ip_address,
            "proxied": True
        }
        zone_id = self.zone_id_from_domain(domain_name)
        try:
            new_record = self.cf.zones.dns_records.post(zone_id, data=dns_record)
            return new_record
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            raise RuntimeError("POST /zones/dns_records %s - api call failed", domain_name) from e

    def dnsrecords_for_domain(self, domain_name: str, ip_address_type: str = "A"):
        zone_id = self.zone_id_from_domain(domain_name)
        try:
            params = {
                "name": domain_name,
                "match": "all",
                "type": ip_address_type,
            }
            dns_records = self.cf.zones.dns_records.get(zone_id, params=params)
            return dns_records
        except cloudflare.exceptions.CloudFlareAPIError as e:
            raise RuntimeError("/zones/dns_records %s - api call failed", domain_name) from e

    def zone_name_from_domain(self, domain_name: str) -> str:
        zone_name = ".".join(domain_name.split(".")[-2:])
        return zone_name

    def zone_id_from_domain(self, domain_name: str) -> str:
        zone_name = self.zone_name_from_domain(domain_name)
        zone_id = self.zone_id_from_name(zone_name)
        return zone_id

    @lru_cache
    def zone_id_from_name(self, zone_name: str) -> str:
        zone_infos = self.cf.zones.get(params={"name": zone_name})

        if not zone_infos:
            raise RuntimeError("No zone named %s found", zone_name)
        elif len(zone_infos) > 1:
            raise RuntimeError("Ambiguous zone name %s", zone_name)

        return zone_infos[0]["id"]

    def ip_address_type(self, ip_address: str) -> str:
        return "AAAA" if ":" in ip_address else "A"


def get_current_ip():
    external_ip = urllib.request.urlopen('https://checkip.amazonaws.com').read().decode('utf8').strip()
    return external_ip


def lookup_ip(domain: str) -> str:
    return socket.gethostbyname(domain)


def logging_configuration(
    verbosity: int, logfile: Path | None = None
) -> dict[str, Any]:
    """
    Build logging configuration based on a verbosity level.

    Args:
        verbosity:
            Verbosity 0 is means "CRITICAL", and each increments move
            toward "DEBUG".
        logfile:
            Optional log file.
    """
    level = max(logging.CRITICAL - logging.DEBUG * verbosity, logging.DEBUG)

    formatters = {
        "standard": {"format": "%(levelname)8s: %(message)s"},
        "debug": {"format": "%(levelname)8s %(name)-24s> %(message)s"},
    }

    active_formatter = "debug" if level < logging.DEBUG else "standard"

    handlers = {
        "console": {
            "level": level,
            "formatter": active_formatter,
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",  # Default is stderr
        },
    }

    if logfile:
        handlers["file"] = {
            "level": level,
            "formatter": active_formatter,
            "class": "logging.FileHandler",
            "filename": str(logfile),
        }

    loggers = {
        "": {
            # root logger
            "handlers": handlers.keys(),
            "level": level,
            "propagate": True,
        }
    }

    return {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": formatters,
        "handlers": handlers,
        "loggers": loggers,
    }


if __name__ == "__main__":
    main()
