Usage
=====

Configure
---------

Copy the ``template.env`` as ``.env`` and ad your own values.
**This file should remain private and contains sensitive information.**


Using podman-compose
--------------------

*You need a working Python installation.*

*These instruction expects a UNIX shell, such as BaSH*

Install podman:

.. code-block:: shell

    sudo apt install podman

Setup an isolated Python environment

.. code-block:: shell

    python -m venv .venv
    source .venv/bin/activate

Install podman-compose in that environment:

.. code-block:: shell

    pip install podman-compose


Run the wokloads:

.. code-block:: shell

    podman-compose up --detach

Stop the workloads:

.. code-block:: shell

    podman-compose down


Serving services
----------------


If you wish to expose services below port 1024, you need to know about
the shortcomings of `Rootless Podman`_, in particular

*   Podman can not create containers that bind to ports < 1024.

    *   The kernel does not allow processes without CAP_NET_BIND_SERVICE
        to bind to low ports.

    *   You can modify the net.ipv4.ip_unprivileged_port_start sysctl to
        change the lowest port.
        For example ``sysctl net.ipv4.ip_unprivileged_port_start=443`` allows
        rootless Podman containers to bind to ports >= 443.

    *   A proxy server, kernel firewall rule, or redirection tool such as
        redir may be used to redirect traffic from a privileged port to an
        unprivileged one (where a podman pod is bound) in a server
        scenario - where a user has access to the root account
        (or setuid on the binary would be an acceptable risk),
        but wants to run the containers as an unprivileged user for enhanced
        security and for a limited number of pre-known ports.


.. _`Rootless Podman`: https://github.com/containers/podman/blob/main/rootless.md
